USE doctopen;
DROP TABLE IF EXISTS `user`;
CREATE TABLE user (
    id_user INTEGER PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL UNIQUE KEY,
    password VARCHAR(100) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `role`;

CREATE TABLE role (
    id_role INTEGER PRIMARY KEY AUTO_INCREMENT,
    role_name VARCHAR(50) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `professional`;

CREATE TABLE professional (
    id_professional INTEGER PRIMARY KEY AUTO_INCREMENT,
    lastname VARCHAR(50) NOT NULL,
    firstname VARCHAR(50) NOT NULL,
    mail VARCHAR(50) NOT NULL,
    address VARCHAR(100),
    phone INT(10),
    activity_description VARCHAR(5000),
    schedule VARCHAR(500),
    id_user INTEGER NOT NULL,
    FOREIGN KEY (id_user) REFERENCES user (id_user)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `customer`;

CREATE TABLE customer (
    id_customer INTEGER PRIMARY KEY AUTO_INCREMENT,
    lastname VARCHAR(50) NOT NULL,
    firstname VARCHAR(50) NOT NULL,
    birthday DATE,
    mail VARCHAR(100) NOT NULL,
    id_user INTEGER NOT NULL,
    FOREIGN KEY (id_user) REFERENCES user (id_user)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `city`;

CREATE TABLE city (
    id_city INTEGER PRIMARY KEY AUTO_INCREMENT,
    zipcode INT(10) NOT NULL,
    city_name VARCHAR(150) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `job`;

CREATE TABLE job (
    id_job INTEGER PRIMARY KEY AUTO_INCREMENT,
    job_name VARCHAR(50) NOT NULL,
    speciality VARCHAR(50)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `payment`;

CREATE TABLE payment (
    id_payment INTEGER PRIMARY KEY AUTO_INCREMENT,
    payment_name VARCHAR(50) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `appointment`;

CREATE TABLE appointment (
    id_appointment INTEGER PRIMARY KEY AUTO_INCREMENT,
    date DATE,
    id_customer INTEGER NOT NULL,
    id_professional INTEGER NOT NULL,
    FOREIGN KEY(id_customer) REFERENCES customer (id_customer),
    FOREIGN KEY(id_professional) REFERENCES professional (id_professional)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE availability (
    id_availability INTEGER PRIMARY KEY AUTO_INCREMENT,
    date DATE,
    disponibility BIT NOT NULL,
    duration INT(11),
    start_time_morning DATE,
    end_time_morning DATE,
    start_time_afternoon DATE,
    end_time_afternoon DATE,
    id_professional INTEGER NOT NULL,
    FOREIGN KEY(id_professional) REFERENCES professional (id_professional)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE user_role (
    id_user INTEGER NOT NULL,
    id_role INTEGER NOT NULL,
    FOREIGN KEY (id_user) REFERENCES user (id_user),
    FOREIGN KEY (id_role) REFERENCES role (id_role)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `professional_job`;

CREATE TABLE professional_job (
    id_professional INTEGER NOT NULL,
    id_job INTEGER NOT NULL,
    FOREIGN KEY (id_professional) REFERENCES professional (id_professional),
    FOREIGN KEY (id_job) REFERENCES job (id_job)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `professional_city`;

CREATE TABLE professional_city (
    id_professional INTEGER NOT NULL,
    id_city INTEGER NOT NULL,
    FOREIGN KEY (id_professional) REFERENCES professional (id_professional),
    FOREIGN KEY (id_city) REFERENCES city (id_city)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `professional_payment`;

CREATE TABLE professional_payment (
    id_professional INTEGER NOT NULL,
    id_payment INTEGER NOT NULL,
    FOREIGN KEY (id_professional) REFERENCES professional (id_professional),
    FOREIGN KEY (id_payment) REFERENCES payment (id_payment)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
