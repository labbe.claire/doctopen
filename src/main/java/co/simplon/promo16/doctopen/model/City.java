package co.simplon.promo16.doctopen.model;

public class City {
    private Integer id_city;
    private int zipcode;
    private String city_name;
    
    public City(int zipcode, String city_name) {
        this.zipcode = zipcode;
        this.city_name = city_name;
    }

    public City(Integer id_city, int i, String city_name) {
        this.id_city = id_city;
        this.zipcode = i;
        this.city_name = city_name;
    }

    public City() {
    }

    public Integer getId_city() {
        return id_city;
    }

    public void setId_city(Integer id_city) {
        this.id_city = id_city;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }
}
