package co.simplon.promo16.doctopen.model;

import java.util.List;

public class Professional {

    private Integer id_professional;
    private String lastName;
    private String firstName;
    private String mail;
    private String address;
    private int phone;
    private String description;
    private String schedule;
    private List<City> cities;
    private List<Payment> payments;
    private List<Job> jobs;
    private List<Availability> availabilities;
    private User user;

    public Professional(Integer id_professional, String lastName, String firstName, String mail, String address,
            int phone, String description, String schedule) {
        this.id_professional = id_professional;
        this.lastName = lastName;
        this.firstName = firstName;
        this.mail = mail;
        this.address = address;
        this.phone = phone;
        this.description = description;
        this.schedule = schedule;
    }

    public Professional(Integer id_professional, String lastName, String firstName, String address) {
        this.id_professional = id_professional;
        this.lastName = lastName;
        this.firstName = firstName;
        this.address = address;
    }

    public Professional(String lastName, String firstName, String mail, String address,
            int phone, String description, String schedule, List<City> cities, List<Payment> payments, List<Job> jobs,
            List<Availability> availabilities) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.mail = mail;
        this.address = address;
        this.phone = phone;
        this.description = description;
        this.schedule = schedule;
        this.cities = cities;
        this.payments = payments;
        this.jobs = jobs;
        this.availabilities = availabilities;
    }

    public Professional(Integer id_professional, String lastName, String firstName, String mail, String address,
            int phone, String description, String schedule, List<City> cities, List<Payment> payments, List<Job> jobs,
            List<Availability> availabilities) {
        this.id_professional = id_professional;
        this.lastName = lastName;
        this.firstName = firstName;
        this.mail = mail;
        this.address = address;
        this.phone = phone;
        this.description = description;
        this.schedule = schedule;
        this.cities = cities;
        this.payments = payments;
        this.jobs = jobs;
        this.availabilities = availabilities;
    }

    public Professional() {
    }

    public Integer getId_professional() {
        return id_professional;
    }

    public void setId_professional(Integer id_professional) {
        this.id_professional = id_professional;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

    public List<Availability> getAvailabilities() {
        return availabilities;
    }

    public void setAvailabilities(List<Availability> availabilities) {
        this.availabilities = availabilities;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    @Override
    public String toString() {
        return "Professional [id = " + id_professional + ", last name=" + lastName + ", first name=" + firstName
                + ", mail=" + mail + ", address=" + address + ", phone=" + phone + ", description=" + description
                + ", schedule=" + schedule + "]";
    }


}
