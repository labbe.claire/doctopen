package co.simplon.promo16.doctopen.model;

public class Payment {
    private Integer id_payment;
    private String payment_name;
    
    public Payment(String payment_name) {
        this.payment_name = payment_name;
    }

    public Payment(Integer id_payment, String payment_name) {
        this.id_payment = id_payment;
        this.payment_name = payment_name;
    }

    public Payment() {
    }

    public Integer getId_payment() {
        return id_payment;
    }

    public void setId_payment(Integer id_payment) {
        this.id_payment = id_payment;
    }

    public String getPayment_name() {
        return payment_name;
    }

    public void setPayment_name(String payment_name) {
        this.payment_name = payment_name;
    }
}
