package co.simplon.promo16.doctopen.model;

import java.time.LocalDate;

public class Availability {
    private Integer id_availability;
    private LocalDate date;
    private boolean disponibility;
    private int duration;
    private LocalDate start_time_morning;
    private LocalDate end_time_morning;
    private LocalDate start_time_afternoon;
    private LocalDate end_time_afternoon;

    public Availability(LocalDate date, boolean disponibility, int duration, LocalDate start_time_morning,
            LocalDate end_time_morning, LocalDate start_time_afternoon, LocalDate end_time_afternoon) {
        this.date = date;
        this.disponibility = disponibility;
        this.duration = duration;
        this.start_time_morning = start_time_morning;
        this.end_time_morning = end_time_morning;
        this.start_time_afternoon = start_time_afternoon;
        this.end_time_afternoon = end_time_afternoon;
    }

    public Availability(Integer id_availability, LocalDate date, boolean disponibility, int duration,
            LocalDate start_time_morning, LocalDate end_time_morning, LocalDate start_time_afternoon,
            LocalDate end_time_afternoon) {
        this.id_availability = id_availability;
        this.date = date;
        this.disponibility = disponibility;
        this.duration = duration;
        this.start_time_morning = start_time_morning;
        this.end_time_morning = end_time_morning;
        this.start_time_afternoon = start_time_afternoon;
        this.end_time_afternoon = end_time_afternoon;
    }

    public Availability(Integer id_availability, LocalDate date, boolean disponibility) {
        this.id_availability = id_availability;
        this.date = date;
        this.disponibility = disponibility;
    }

    public Availability() {
    }

    public Integer getId_availability() {
        return id_availability;
    }

    public void setId_availability(Integer id_availability) {
        this.id_availability = id_availability;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean getDisponibility() {
        return disponibility;
    }

    public void setDisponibility(boolean disponibility) {
        this.disponibility = disponibility;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public LocalDate getStart_time_morning() {
        return start_time_morning;
    }

    public void setStart_time_morning(LocalDate start_time_morning) {
        this.start_time_morning = start_time_morning;
    }

    public LocalDate getEnd_time_morning() {
        return end_time_morning;
    }

    public void setEnd_time_morning(LocalDate end_time_morning) {
        this.end_time_morning = end_time_morning;
    }

    public LocalDate getStart_time_afternoon() {
        return start_time_afternoon;
    }

    public void setStart_time_afternoon(LocalDate start_time_afternoon) {
        this.start_time_afternoon = start_time_afternoon;
    }

    public LocalDate getEnd_time_afternoon() {
        return end_time_afternoon;
    }

    public void setEnd_time_afternoon(LocalDate end_time_afternoon) {
        this.end_time_afternoon = end_time_afternoon;
    }
}
