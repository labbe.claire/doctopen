package co.simplon.promo16.doctopen.model;

import java.time.LocalDate;

public class Appointment {
    private Integer id_appointment;
    private LocalDate date;

    public Appointment(Integer id_appointment, LocalDate date) {
        this.id_appointment = id_appointment;
        this.date = date;
    }

    public Appointment(LocalDate date) {
        this.date = date;
    }

    public Appointment() {
    }

    public Integer getId_appointment() {
        return id_appointment;
    }

    public void setId_appointment(Integer id_appointment) {
        this.id_appointment = id_appointment;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

}
