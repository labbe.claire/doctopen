package co.simplon.promo16.doctopen.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.dao.PaymentMethodDAO;
import co.simplon.promo16.doctopen.model.Payment;

@Repository
public class PaymentMethodDAOImpl implements PaymentMethodDAO {
    private static final String CREATE_PAYMENT_METHOD = "INSERT INTO payment (payment_name) VALUES (?);";
    private static final String GET_PAYMENT_METHODS = "SELECT * FROM payment;";
    private static final String GET_PAYMENT_METHOD_BY_ID = "SELECT * FROM payment WHERE id_payment=?;";
    private static final String UPDATE_PAYMENT_METHOD_BY_ID = "UPDATE payment payment_name=? WHERE id=?;";
    private static final String DELETE_PAYMENT_METHOD_BY_ID = "DELETE FROM payment WHERE id_payment=?;";

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @Override
    public List<Payment> findAll() {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_PAYMENT_METHODS);
            ResultSet result = stmt.executeQuery();
            List<Payment> paymentList = new ArrayList<>();
            while (result.next()) {
                Payment payment = new Payment(
                        result.getInt("id_payment"),
                        result.getString("payment_name"));
                paymentList.add(payment);
            }
            connection.close();
            return paymentList;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;

    }

    @Override
    public Payment findById(Integer id_payment) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_PAYMENT_METHOD_BY_ID);
            stmt.setInt(1, id_payment);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Payment payment = new Payment(
                        result.getInt("id_payment"),
                        result.getString("payment_name"));
                connection.close();
                return payment;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseEntity<String> save(Payment payment) {
        try {
            connection = dataSource.getConnection();
            if (payment.getId_payment() != null) {
                return update(payment);
            }
            PreparedStatement stmt = connection.prepareStatement(CREATE_PAYMENT_METHOD);
            stmt.setString(1, payment.getPayment_name());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Payment method successfully created.", HttpStatus.OK);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Payment method can't be created.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> update(Payment payment) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(UPDATE_PAYMENT_METHOD_BY_ID);
            stmt.setString(1, payment.getPayment_name());
            stmt.setInt(2, payment.getId_payment());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Payment method successfully updated.", HttpStatus.OK);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Payment method can't be updated.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(DELETE_PAYMENT_METHOD_BY_ID);
            stmt.setInt(1, id);

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Payment method successfully deleted.", HttpStatus.OK);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>("Error -> Payment method can't be deleted.", HttpStatus.BAD_REQUEST);
    }

}
