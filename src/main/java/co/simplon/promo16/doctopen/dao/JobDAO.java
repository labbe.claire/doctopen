package co.simplon.promo16.doctopen.dao;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.model.Job;

@Repository
public interface JobDAO {
    
    public List<Job> findAll(); 
    public Job findById(Integer id);
    public ResponseEntity<String> save(Job job);
    public ResponseEntity<String> update(Job job);
    public ResponseEntity<String> deleteById(Integer id);

}
