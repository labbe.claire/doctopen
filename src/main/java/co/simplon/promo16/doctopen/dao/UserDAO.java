package co.simplon.promo16.doctopen.dao;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.model.Role;
import co.simplon.promo16.doctopen.model.User;

@Repository
public interface UserDAO {
    
    public List<User> findAll(); 
    public User findById(Integer id);
    public ResponseEntity<String> save(User user, Role role);
    public ResponseEntity<String> update(User user);
    public ResponseEntity<String> deleteById(Integer id);
}
