package co.simplon.promo16.doctopen.dao;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.model.City;

@Repository
public interface CityDAO {
 
    public List<City> findAll();
    public City findById(Integer id);
    public ResponseEntity<String> save(City city);
    public ResponseEntity<String> update(City city);
    public ResponseEntity<String> deleteById(Integer id);

}
