package co.simplon.promo16.doctopen.dao;

import java.util.List;

import org.springframework.http.ResponseEntity;

import co.simplon.promo16.doctopen.model.Appointment;

public interface AppointmentDAO {
    
    public List<Appointment> findAll();
    public Appointment findById(Integer id);
    public ResponseEntity<String> save(Appointment appointment);
    public ResponseEntity<String> update(Appointment appointment);
    public ResponseEntity<String> deleteById(Integer id);

}
