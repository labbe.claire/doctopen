package co.simplon.promo16.doctopen.dao;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.model.Customer;

@Repository
public interface CustomerDAO {
    
    public List<Customer> findAll(); 
    public Customer findById(Integer id);
    public ResponseEntity<String> save(Customer customer);
    public ResponseEntity<String> update(Customer customer);
    public ResponseEntity<String> deleteById(Integer id);

}
