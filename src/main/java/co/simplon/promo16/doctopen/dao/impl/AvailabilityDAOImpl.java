package co.simplon.promo16.doctopen.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.dao.AvailabilityDAO;
import co.simplon.promo16.doctopen.model.Availability;

@Repository
public class AvailabilityDAOImpl implements AvailabilityDAO {
    private static final String CREATE_AVAILABILITY = "INSERT INTO availability (date, disponibility, duration, start_time_morning, end_time_morning, start_time_afternoon, end_time_afternoon) VALUES (?, ?, ?, ?, ?, ?, ?);";
    private static final String GET_AVAILABILITYS = "SELECT * FROM availability;";
    private static final String GET_AVAILABILITY_BY_ID = "SELECT * FROM availability WHERE id_availability = ?";
    private static final String UPDATE_AVAILABILITY_BY_ID = "UPDATE availability SET date = ?, disponibility = ?, duration = ?, start_time_morning = ?, end_time_morning = ?, start_time_afternoon = ?, end_time_afternoon = ? WHERE id=?;";
    private static final String DELETE_AVAILABILITY_BY_ID = "DELETE FROM availability WHERE id_availability=?;";

    @Autowired
    DataSource dataSource;

    private Connection connection;

    @Override
    public List<Availability> findAll() {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_AVAILABILITYS);
            ResultSet result = stmt.executeQuery();
            List<Availability> availabilityList = new ArrayList<>();
            while (result.next()) {
                Availability availability = new Availability(
                        result.getInt("id_availability"),
                        result.getDate("date").toLocalDate(),
                        result.getBoolean("disponibility"));
                availabilityList.add(availability);
            }
            connection.close();
            return availabilityList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Availability findById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_AVAILABILITY_BY_ID);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Availability availability = new Availability(
                        result.getInt("id_availability"),
                        result.getDate("date").toLocalDate(),
                        result.getBoolean("disponibility"),
                        result.getInt("duration"),
                        result.getDate("start_time_morning").toLocalDate(),
                        result.getDate("end_time_morning").toLocalDate(),
                        result.getDate("start_time_afternoon").toLocalDate(),
                        result.getDate("end_time_afternoon").toLocalDate());
                connection.close();
                return availability;
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseEntity<String> save(Availability availability) {
        try {
            connection = dataSource.getConnection();
            if (availability.getId_availability() != null) {
                return update(availability);
            }
            PreparedStatement stmt = connection.prepareStatement(CREATE_AVAILABILITY);
            stmt.setDate(1, Date.valueOf(availability.getDate()));
            stmt.setBoolean(2, availability.getDisponibility());
            stmt.setInt(3, availability.getDuration());
            stmt.setDate(4, Date.valueOf(availability.getStart_time_morning()));
            stmt.setDate(5, Date.valueOf(availability.getEnd_time_morning()));
            stmt.setDate(6, Date.valueOf(availability.getStart_time_afternoon()));
            stmt.setDate(7, Date.valueOf(availability.getEnd_time_afternoon()));
            int execute = stmt.executeUpdate();

            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Availability successfully created.", HttpStatus.OK);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Availability can't be created.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> update(Availability availability) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(UPDATE_AVAILABILITY_BY_ID);
            stmt.setDate(1, Date.valueOf(availability.getDate()));
            stmt.setBoolean(2, availability.getDisponibility());
            stmt.setInt(3, availability.getDuration());
            stmt.setDate(4, Date.valueOf(availability.getStart_time_morning()));
            stmt.setDate(5, Date.valueOf(availability.getEnd_time_morning()));
            stmt.setDate(5, Date.valueOf(availability.getStart_time_afternoon()));
            stmt.setDate(5, Date.valueOf(availability.getEnd_time_afternoon()));
            stmt.setInt(8, availability.getId_availability());
            int execute = stmt.executeUpdate();

            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Availability successfully updated.", HttpStatus.OK);
            }
            ;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Availability can't be updated.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(DELETE_AVAILABILITY_BY_ID);
            stmt.setInt(1, id);
            
            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Availability successfully deleted.", HttpStatus.OK);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Availability can't be deleted.", HttpStatus.BAD_REQUEST);
    }
}
