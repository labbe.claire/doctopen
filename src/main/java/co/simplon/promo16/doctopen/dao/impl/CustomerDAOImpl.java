package co.simplon.promo16.doctopen.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.dao.CustomerDAO;
import co.simplon.promo16.doctopen.model.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {
    private static final String CREATE_CUSTOMER = "INSERT INTO customer(firstName,lastName,birthday,mail,id_user) VALUES (?,?,?,?)";
    private static final String GET_CUSTOMERS = "SELECT * FROM customer;";
    private static final String GET_CUSTOMER_BY_ID = "SELECT * FROM customer WHERE id_customer=?;";
    private static final String UPDATE_CUSTOMER_BY_ID = "UPDATE customer SET firstName=? lastName=? birthday=? mail=? WHERE id_customer=?;";
    private static final String DELETE_CUSTOMER_BY_ID = "DELETE FROM customer WHERE id_customer=?;";

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @Override
    public List<Customer> findAll() {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_CUSTOMERS);
            ResultSet result = stmt.executeQuery();
            List<Customer> customerList = new ArrayList<>();
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("id_customer"),
                        result.getString("firstName"),
                        result.getString("lastName"),
                        result.getDate("birthday").toLocalDate(),
                        result.getString("mail"));
                customerList.add(customer);
            }
            connection.close();
            return customerList;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Customer findById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_CUSTOMER_BY_ID);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Customer customer = new Customer(
                        result.getInt("id_customer"),
                        result.getString("firstName"),
                        result.getString("lastName"),
                        result.getDate("birthday").toLocalDate(),
                        result.getString("mail"));
                connection.close();
                return customer;
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseEntity<String> save(Customer customer) {
        try {
            connection = dataSource.getConnection();
            if (customer.getId_customer() != null) {
                update(customer);
            }
            PreparedStatement stmt = connection
                    .prepareStatement(CREATE_CUSTOMER);
            stmt.setString(1, customer.getFirstName());
            stmt.setString(2, customer.getLastName());
            stmt.setDate(3, Date.valueOf(customer.getBirthday()));
            stmt.setString(4, customer.getMail());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Customer successfully created.", HttpStatus.OK);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Customer can't be created.", HttpStatus.BAD_REQUEST);

    }

    @Override
    public ResponseEntity<String> update(Customer customer) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement(UPDATE_CUSTOMER_BY_ID);
            stmt.setString(1, customer.getFirstName());
            stmt.setString(2, customer.getLastName());
            stmt.setDate(3, Date.valueOf(customer.getBirthday()));
            stmt.setString(4, customer.getMail());
            stmt.setInt(5, customer.getId_customer());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Customer successfully updated.", HttpStatus.OK);
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Customer can't be updated.", HttpStatus.BAD_REQUEST);

    }

    @Override
    public ResponseEntity<String> deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(DELETE_CUSTOMER_BY_ID);
            stmt.setInt(1, id);

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Customer successfully deleted.", HttpStatus.OK);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>("Error -> Customer can't be deleted.", HttpStatus.BAD_REQUEST);
    }
}
