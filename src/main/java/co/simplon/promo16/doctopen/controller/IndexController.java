package co.simplon.promo16.doctopen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.promo16.doctopen.dao.CityDAO;
import co.simplon.promo16.doctopen.dao.JobDAO;

@Controller
public class IndexController {

    @Autowired
    public CityDAO cityDAOImpl;

    @Autowired
    public JobDAO jobDAOImpl;

    @GetMapping("/")
    public String getProfessionnalByCity(Model model, Model model2) {
        model.addAttribute("city", cityDAOImpl.findAll());
        model2.addAttribute("job", jobDAOImpl.findAll());
        return "index";

    }

}
