package co.simplon.promo16.doctopen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoctopenApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoctopenApplication.class, args);
	}

}
